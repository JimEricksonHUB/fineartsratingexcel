﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Configuration;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Drawing;
using DuckCreek.DCTServer.Lib;
using DuckCreek.SessionObj;
using DuckCreek.DCTServer.Interfaces;
using OfficeOpenXml;

namespace CustomExcel {
    public class RatingExcel : DCTBaseObj {
        #region Class Fields

        private string _pathToExcelFileSave = string.Empty;
        private string _templatePrivateXL = string.Empty;
        private string _templateInstitutionXL = string.Empty;
        private string _templateCommercialXL = string.Empty;

        #endregion

        #region Constructors

        public RatingExcel() {
            Init();
        }

        #endregion

        #region Public Methods

        public string PopulateXLExcel(XmlElement requestElem, Session session) {

            // EPPlus Licensing
            ExcelPackage.LicenseContext = LicenseContext.Commercial;

            XmlDocument currentSession = session.getXmlDocument();

            string PolicyType = requestElem.GetAttribute("PolicyType");
            string RiskState = requestElem.GetAttribute("RiskState");
            string Insured = requestElem.GetAttribute("Insured");
            Insured = Insured.Replace("&","and");
            Insured = Insured.Replace("/","");
            Insured = Insured.Replace("\\","");
            string PolicyNumber = requestElem.GetAttribute("PolicyNumber");
            string InceptionDate = requestElem.GetAttribute("InceptionDate");
            string ExpirationDate = requestElem.GetAttribute("ExpirationDate");
            string Broker = requestElem.GetAttribute("Broker");
            string Commission = requestElem.GetAttribute("Commission");

            // Primary Limits Node
            XmlNodeList primaryLimitNodes = currentSession.SelectNodes("//session/data/policy/line/risk/riskCoverages/primaryliabilitylimit");

            // Personal Articles Node
            XmlNodeList personalArticlesNodes = currentSession.SelectNodes("//session/data/policy/line/risk/personalarticles");

            // Deductible Node
            XmlNodeList deductibleNodes = currentSession.SelectNodes("//session/data/policy/line/risk/riskCoverages/deductible");
            
            string TemplateFileName = String.Empty;
            string columnLetter = String.Empty;
            string columnClass = String.Empty;
            string columnCoverage = String.Empty;
            string columnLimit = String.Empty;
            string columnDeductible = String.Empty;
            int rowState = 0;
            int rowInsured = 0;
            int rowPolicyNumber = 0;
            int rowInception = 0;
            int rowExpiry = 0;
            int rowBroker = 0;
            int rowCommission = 0;
            int rowPrimaryLimit = 0;
            int rowDeductible = 0;

            // Load proper template based on Policy Type
            // Case Policy Type = Fine Art Collector or Personal Articles Floater
            if (PolicyType == "FineArtCollector" || PolicyType == "PersonalArticles") {
                TemplateFileName = _templatePrivateXL;
                columnClass = "E";
                columnLetter = "F";
                columnLimit = "F";
                columnDeductible = "G";
                rowState = 2;
                rowInsured = 5;
                rowPolicyNumber = 6;
                rowInception = 7;
                rowExpiry = 8;
                rowBroker = 9;
                rowCommission = 10;
                rowPrimaryLimit = 30;
                rowDeductible = 30;
            }
            else if (PolicyType == "Museum" || PolicyType == "Exhibition") {
                TemplateFileName = _templateInstitutionXL;
                columnLetter = "F";
                columnLimit = "F";
                columnDeductible = "H";
                rowState = 2;
                rowInsured = 5;
                rowPolicyNumber = 6;
                rowInception = 7;
                rowExpiry = 8;
                rowBroker = 9;
                rowCommission = 10;
                rowPrimaryLimit = 32;
                rowDeductible = 32;
            }
            else if (PolicyType == "FineArtDealer" || PolicyType == "CommercialArtist" || PolicyType == "CoinStampDealer") {
                TemplateFileName = _templateCommercialXL;
                columnLetter = "E";
                columnClass = "E";
                columnCoverage = "F";
                columnLimit = "G";
                columnDeductible = "H";
                rowState = 4;
                rowInsured = 8;
                rowPolicyNumber = 9;
                rowInception = 10;
                rowExpiry = 11;
                rowBroker = 12;
                rowCommission = 13;
                rowPrimaryLimit = 28;
                rowDeductible = 28;
            }

            FileInfo templateFile = new FileInfo(TemplateFileName);
            FileInfo newFile = new FileInfo(_pathToExcelFileSave + Insured + "_" + PolicyNumber + "_XLRating.xlsm");
            using (ExcelPackage package = new ExcelPackage(newFile, templateFile)) {
                ExcelWorksheet ws = package.Workbook.Worksheets[0];
                ws.Cells[columnLetter + rowState].Value = RiskState;
                ws.Cells[columnLetter + rowInsured].Value = Insured;
                ws.Cells[columnLetter + rowPolicyNumber].Value = PolicyNumber;
                ws.Cells[columnLetter + rowInception].Value = InceptionDate;
                ws.Cells[columnLetter + rowExpiry].Value = ExpirationDate;
                ws.Cells[columnLetter + rowBroker].Value = Broker;
                ws.Cells[columnLetter + rowCommission].Value = Commission;

                if (PolicyType != "PersonalArticles") {
                    foreach (XmlNode xml in primaryLimitNodes) {
                        XmlNode limitNode = xml.SelectSingleNode("Limit");
                        if (limitNode.InnerText != null) {
                            ws.Cells[columnLimit + rowPrimaryLimit].Value = int.Parse(limitNode.InnerText);
                            if (TemplateFileName == _templateCommercialXL) {
                                ws.Cells[columnClass + rowPrimaryLimit].Value = "Other Property";
                                ws.Cells[columnCoverage + rowPrimaryLimit].Value = "All";
                            }
                            rowPrimaryLimit++;
                        }
                    }
                }
                else {
                    foreach (XmlNode xml in personalArticlesNodes) {
                        XmlNode personalArticlesLimitNode = xml.SelectSingleNode("Limit");
                        if (personalArticlesLimitNode.InnerText != null) {
                            ws.Cells[columnLimit + rowPrimaryLimit].Value = int.Parse(personalArticlesLimitNode.InnerText);
                        }
                        XmlNode personalArticlesTypeNode = xml.SelectSingleNode("Type");
                        if (personalArticlesTypeNode.InnerText != null) {
                            ws.Cells[columnClass + rowPrimaryLimit].Value = personalArticlesTypeNode.InnerText;
                        }
                        rowPrimaryLimit++;
                    }
                }

                if (deductibleNodes.Count > 1) {
                    if (deductibleNodes.Item(0).InnerText != null) {
                        ws.Cells[columnDeductible + rowDeductible].Value = deductibleNodes.Item(0).InnerText;
                    }
                }
                package.Save();
            }

            string response = "Rating Form Saved Here : " + newFile;
            session.setElement("data/policy/XLRatingFormSavePath", response);
            return response;
           // session.setLeaf("/dataStore/session/data/policy/XLRatingFormSavePath", response);
        }

        #endregion

        #region Private Methods

        private void Init() {
            ReadAppSettings();
        }

        private void ReadAppSettings() {
            try {
                _pathToExcelFileSave = ConfigurationManager.AppSettings["HUB.Integrations.RatingExcel.PathToExcelFiles"];
                _templatePrivateXL = ConfigurationManager.AppSettings["HUB.Integrations.RatingExcel.PrivateXLTemplate"];
                _templateInstitutionXL = ConfigurationManager.AppSettings["HUB.Integrations.RatingExcel.InstitutionXLTemplate"];
                _templateCommercialXL = ConfigurationManager.AppSettings["HUB.Integrations.RatingExcel.CommercialXLTemplate"];
            }
            catch (Exception) {
                Console.WriteLine("Unexpected error reading configuration file information: ");
            }
        }
        #endregion



    }
}